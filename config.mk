#
#
# Miui camera for AOSP ROMs
#
#

PRODUCT_SOONG_NAMESPACES += \
    vendor/xiaomi/miuicam


CAM_PATH := $(LOCAL_PATH)

# Permissions & libs
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/system_ext/lib64,$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64) \
    $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/vendor/bin/hw,$(TARGET_COPY_OUT_VENDOR)/bin/hw) \
      $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/vendor/lib,$(TARGET_COPY_OUT_VENDOR)/lib) \
      $(call find-copy-subdir-files,*,$(LOCAL_PATH)/proprietary/vendor/lib64,$(TARGET_COPY_OUT_VENDOR)/lib64)



# Meme cam
PRODUCT_PACKAGES += \
MiuiCamera

# Props needed
ro.miui.notch=1 \
ro.miui.build.region=IN \ 
ro.com.google.lens.oem_camera_package=com.android.camera \
vendor.camera.aux.packagelist=org.codeaurora.snapcam,com.android.camera \
persist.vendor.camera.privapp.list=org.codeaurora.snapcam,com.android.camera \


#lib for miuicam
PRODUCT_COPY_FILES += \
    vendor/xiaomi/miuicam/proprietary/libs/libcamera_algoup_jni.xiaomi.so:$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib/arm64/libcamera_algoup_jni.xiaomi.so \
    vendor/xiaomi/miuicam/proprietary/libs/libcamera_mianode_jni.xiaomi.so:$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib/arm64/libcamera_mianode_jni.xiaomi.so

include $(CAM_PATH)/BoardConfigMIUI.mk
